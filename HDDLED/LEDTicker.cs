﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Collections.Specialized;
using System.Threading;
using System.Diagnostics;

namespace HDDLED
{
    public partial class LEDTicker : Form
    {
        //global variables here
        NotifyIcon hddLedIcon;
        Icon activeIcon;
        Icon idleIcon;
        Thread hddLedWorker;

        //Disk running on current PC    
        PerformanceCounter DiskWrite = new PerformanceCounter("LogicalDisk", "Disk Write Bytes/sec", "_Total");
        PerformanceCounter DiskRead = new PerformanceCounter("LogicalDisk", "Disk Read Bytes/sec", "_Total");
        PerformanceCounter DiskIdle = new PerformanceCounter("LogicalDisk", "% Idle Time", "_Total");


        public LEDTicker()
        {
            //init the form
            InitializeComponent();

            //icons init
            activeIcon = new Icon("HDDBusy.ico");
            idleIcon = new Icon("HDDIdle.ico");

            //Create notify icon, assign and show it
            hddLedIcon = new NotifyIcon();
            hddLedIcon.Icon = idleIcon;
            hddLedIcon.Visible = true;

            //menu items
            MenuItem closeMenuItem = new MenuItem("Exit");
            MenuItem progName = new MenuItem("HDD Activity V0.01 BETA - Torean");
            ContextMenu menu = new ContextMenu();

            //get all processes
            Process[] localAll = Process.GetProcesses();

            //add menu to context
            menu.MenuItems.Add(progName);
            menu.MenuItems.Add(closeMenuItem);

            //add context to notify icon
            hddLedIcon.ContextMenu = menu;

            //event click handlers for menu item (close app when click)
            closeMenuItem.Click += CloseMenuItem_Click;

            //hide the form window
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;

            //start the thread to pull HDD activity
            hddLedWorker = new Thread(new ThreadStart(HDDActivityThread));
            hddLedWorker.Start();
        }

        //click even to close hardrive
        private void CloseMenuItem_Click(object sender, EventArgs e)
        {
            //stop thread
            hddLedWorker.Abort();

            //dispose icons
            hddLedIcon.Dispose();

            //close app on click
            this.Close();
        }

        //threads - process that runs paralell with the program running
        public void HDDActivityThread()
        {
            //class to get the drive data to get the WMI nameSpace
            ManagementClass driveClassData = new ManagementClass("Win32_PerfFormattedData_PerfDisk_PhysicalDisk");

            try
            {
                //main loop to run thread - it will consume all cpu if you dont sleep it
                while (true)
                {
                    //get collection data from drive data
                    ManagementObjectCollection driveDataClassCollection = driveClassData.GetInstances();

                    //update the usage of the HDD indicator
                    hddLedIcon.Text = "Activity: " + (100 - Math.Round(DiskIdle.NextValue())) + "%" + 
                                        "\nWrite: " + Math.Round(DiskWrite.NextValue() / 1000000) + " mb/sec" + 
                                        "\nRead: " + Math.Round(DiskRead.NextValue() / 1000000) + " mb/sec";

                    foreach (ManagementObject obj in driveDataClassCollection) {
                        //only prcess total instance and ignore other
                        if ( obj["Name"].ToString() == "_Total" ) {
                            if (Convert.ToUInt64(obj["DiskReadBytesPersec"]) > 0)
                            {
                                //show busy icon
                                hddLedIcon.Icon = activeIcon;

                            }
                            else
                            {
                                //show idle icon
                                hddLedIcon.Icon = idleIcon;
                            }
                        }
                    }

                    //sleep thread to not take up all the usage to run the thread, sleep every 10th of a millisec
                    Thread.Sleep(100);
                }
            }
            catch (ThreadInterruptedException e) {
                //thread was closed and aborted
                driveClassData.Dispose(); ;
            }
           
            
        }
    }
}
